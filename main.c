#include <stdio.h>
#include "type.h"

float areaRectangle(const rectangle rectangle) {
    return rectangle.a * rectangle.b;
}

float areaSquare(const square square) {
    rectangle rect;
    rect.a = square.a;
    rect.b = square.a;
    return areaRectangle(rect);
}

float perimeterRectangle(const rectangle rectangle) {
    return rectangle.a * 2 + 2 * rectangle.b;
}

float perimeterSquare(const square square) {
    rectangle rect;
    rect.a = square.a;
    rect.b = square.a;
    return areaRectangle(rect);
}

int main(void) {
    // square sq;
    // sq.a = 1.5;
    // rectangle rec;
    // rec.a = 1.5;
    // rec.b = 3;
    // const float areaSq = areaSquare(sq);
    // const float areaRec = areaRectangle(rec);
    // const float perimeterSq = perimeterSquare(sq);
    // const float perimeterRec = perimeterRectangle(rec);
    //
    // printf("Area square %f | rectangle %f\n", areaSq, areaRec);
    // printf("Perimeter square %f | rectangle %f\n", perimeterSq, perimeterRec);

    //Translation Point
    Point pt = initPoint(1, 1);
    printPoint(pt);
    translatePoint(&pt, 2, 0);
    printPoint(pt);

    //Translation Quadrinome
    Quadrinome quadrinome = initQuadrinome(
        1, 1, 1, 2, 3, 2, 3, 1
    );
    printQuadrinome(quadrinome);
    translateQuaddrinome(&quadrinome, 2, 0);
    printQuadrinome(quadrinome);




    return 0;
}
