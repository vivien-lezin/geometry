//
// Created by vivie on 23/04/2024.
//

#include "type.h"

#include <stdio.h>

Point initPoint(const float x, const float y) {
    Point pt;
    pt.x = x;
    pt.y = y;
    return pt;
}

Vector initVector(const Point pointA, const Point pointB) {
    Vector v;
    v.pointA = pointA;
    v.pointB = pointB;
    return v;
}

Vector initVectorPoint(
    const float pointA_x,
    const float pointA_y,
    const float pointB_x,
    const float pointB_y) {
    return initVector(
        initPoint(pointA_x, pointA_y),
        initPoint(pointB_x, pointB_y));
}

Quadrinome initQuadrinome(
    const float pointA_x,
    const float pointA_y,
    const float pointB_x,
    const float pointB_y,
    const float pointC_x,
    const float pointC_y,
    const float pointD_x,
    const float pointD_y
) {
    Quadrinome quad;
    quad.pointA = initPoint(pointA_x,
                            pointA_y);
    quad.pointB = initPoint(pointB_x,
                            pointB_y);
    quad.pointC = initPoint(pointC_x,
                            pointC_y);
    quad.pointD = initPoint(pointD_x,
                            pointD_y);
    return quad;
}


void translatePoint(Point *point, const float x, const float y) {
    point->x+= x;
    point->y+= y;
}

void translateQuaddrinome(Quadrinome *quad, const int x, const int y) {
    translatePoint(&quad->pointA, x, y);
    translatePoint(&quad->pointB, x, y);
    translatePoint(&quad->pointC, x, y);
    translatePoint(&quad->pointD, x, y);
}

void printPoint(const Point pt) {
    printf("x: %f - y: %f\n", pt.x, pt.y);
}

void printQuadrinome(const Quadrinome quad) {
    printf("Point A ");
    printPoint(quad.pointA);
    printf("Point B ");
    printPoint(quad.pointB);
    printf("Point C ");
    printPoint(quad.pointC);
    printf("Point D ");
    printPoint(quad.pointD);
}