//
// Created by vivie on 23/04/2024.
//

#ifndef TYPE_H
#define TYPE_H
struct __square {
    float a;
};
typedef struct __square square;

struct __rectangle {
    float a;
    float b;
};
typedef struct __rectangle rectangle;


struct __point {
    float x;
    float y;
};
typedef struct __point Point;
struct __vector {
    Point pointA;
    Point pointB;
};
typedef struct __vector Vector;

Vector initVectorPoint(
    const float pointA_x,
    const float pointA_y,
    const float pointB_x,
    const float pointB_y);
struct __quadrinome {
    Point pointA;
    Point pointB;
    Point pointC;
    Point pointD;
};
typedef struct __quadrinome Quadrinome;
Quadrinome initQuadrinome(
    const float pointA_x,
    const float pointA_y,
    const float pointB_x,
    const float pointB_y,
    const float pointC_x,
    const float pointC_y,
    const float pointD_x,
    const float pointD_y
);
Point initPoint(const float x, const float y);

void translatePoint(Point *point, float x, float y);

void translateQuaddrinome(Quadrinome *quad, int x, int y);

void printPoint(const Point pt);

void printQuadrinome(const Quadrinome quad);

#endif //TYPE_H
